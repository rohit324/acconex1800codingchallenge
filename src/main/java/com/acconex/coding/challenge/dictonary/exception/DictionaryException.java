package com.acconex.coding.challenge.dictonary.exception;

import com.acconex.coding.challenge.exception.CodingChallengeException;

/**
 * Created by rohit on 28/07/16.
 */
public class DictionaryException extends CodingChallengeException {

    public DictionaryException(Throwable cause) {
        super(cause);
    }

    public DictionaryException(String message) {
        super(message);
    }
}
