package com.acconex.coding.challenge.dictonary;

import com.acconex.coding.challenge.dictonary.exception.DictionaryException;

import java.util.Set;

/**
 * Created by rohit on 25/07/16.
 */
public interface IDictionaryStore {

    void loadDictionary() throws DictionaryException;

    Set<String> getAllWordsForLength(int length);

    boolean isLoaded();

}
