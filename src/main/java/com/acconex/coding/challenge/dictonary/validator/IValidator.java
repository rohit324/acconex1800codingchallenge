package com.acconex.coding.challenge.dictonary.validator;

import com.acconex.coding.challenge.dictonary.exception.DictionaryValidaltionException;

/**
 * Created by rohit on 30/07/16.
 */
public interface IValidator {

    public void validateInputData(String inputLine) throws DictionaryValidaltionException;
}
