package com.acconex.coding.challenge.dictonary.facade;

import com.acconex.coding.challenge.dictonary.exception.DictionaryException;

/**
 * Created by rohit on 25/07/16.
 */
public class DictionaryManager {

    private static DictionaryManager instance = new DictionaryManager();
    private static Dictionary dictionary = new DictionaryImpl(Dictionary.defaultDictionaryPath);

    // Lets load the default dictionary in memory.
    static {
        try {
            dictionary.loadDictionary();
        } catch (DictionaryException e) {
            throw new RuntimeException(e);
        }
    }

    private DictionaryManager(){

    }

    public static DictionaryManager getInstance() {
        return instance;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public synchronized void loadNewDictionary(String dictionaryPath) throws DictionaryException {
        DictionaryImpl newDictionary = new DictionaryImpl(dictionaryPath);
        newDictionary.loadDictionary();
        dictionary = newDictionary;
    }

    public void resetDictionary() throws DictionaryException {
        dictionary = new DictionaryImpl(Dictionary.defaultDictionaryPath);
        dictionary.loadDictionary();
    }
}
