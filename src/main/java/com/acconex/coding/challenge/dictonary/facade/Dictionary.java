package com.acconex.coding.challenge.dictonary.facade;

import com.acconex.coding.challenge.dictonary.exception.DictionaryException;

import java.util.Set;

/**
 * Created by rohit on 25/07/16.
 */
public interface Dictionary {

   public static String defaultDictionaryPath = "config/dictionary/defaultDictionary.txt";

   Set<String> getAllWordsOfLength(int length);

   void reloadDictionary() throws DictionaryException;

   void loadDictionary() throws  DictionaryException;
}
