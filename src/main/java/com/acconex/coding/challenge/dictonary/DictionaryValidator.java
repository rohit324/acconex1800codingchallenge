package com.acconex.coding.challenge.dictonary;

import com.acconex.coding.challenge.dictonary.validator.IValidator;
import com.acconex.coding.challenge.dictonary.validator.Validator;

/**
 * Created by rohit on 30/07/16.
 */
public class DictionaryValidator {

    private static DictionaryValidator instance = new DictionaryValidator();
    private static Validator validator = new Validator();
    private DictionaryValidator() {

    }

    public static DictionaryValidator getInstance(){
        return instance;
    }

    public IValidator getValidator() {
        return validator;
    }
}
