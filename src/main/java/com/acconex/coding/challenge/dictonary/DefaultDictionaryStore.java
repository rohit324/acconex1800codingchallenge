package com.acconex.coding.challenge.dictonary;

import com.acconex.coding.challenge.dictonary.validator.IValidator;
import com.acconex.coding.challenge.exception.CodingChallengeRuntimeException;
import com.acconex.coding.challenge.dictonary.exception.DictionaryException;
import com.acconex.coding.challenge.utils.CodingChallengeUtils;
import com.acconex.coding.challenge.utils.FileUtils;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by rohit on 25/07/16.
 */
public class DefaultDictionaryStore implements IDictionaryStore {

    // todo :- multiple dictionary file uplaods can cause memory leak with this type of store
    // todo :- a lucene store will be better to handle dictionary of very large sizes.
    private String dictionaryPath;
    private Set<String> dictionaryData = new HashSet<>();
    private Map<Integer,Set<String>> wordLengthMap = new HashMap<>();
    private boolean isLoaded = false;

    public DefaultDictionaryStore(String dictionaryPath) {
        this.dictionaryPath = dictionaryPath;
    }

    public void loadDictionary() throws DictionaryException {
        String absolutePath = null;
        try {
            absolutePath = FileUtils.getAbsolutePath(dictionaryPath);
        } catch (FileNotFoundException e) {
            throw new DictionaryException(e);
        }
        dictionaryData  = getDictionaryData(absolutePath);
        processDictionaryData();
    }

    private HashSet<String> getDictionaryData(String absolutePath) throws DictionaryException {
        File dicFile = new File(absolutePath);
        HashSet<String> readLines = new HashSet<>();
        FileReader reader = null;
        try {
            reader = new FileReader(dicFile);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String readLine;
            while((readLine = bufferedReader.readLine())!=null) {
                IValidator validator = DictionaryValidator.getInstance().getValidator();
                validator.validateInputData(readLine);
                readLines.add(readLine);
            }
            return readLines;
        } catch (IOException e) {
            throw new DictionaryException(e);
        } finally {
            if(reader!=null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new DictionaryException(e);
                }
            }
        }
    }

    @Override
    public Set<String> getAllWordsForLength(int length) {
        if(!isLoaded) {
            throw new CodingChallengeRuntimeException("Dictionary has not been loaded");
        }
        return wordLengthMap.get(length);
    }

    @Override
    public boolean isLoaded() {
        return isLoaded;
    }

    // TODO :- add lucene indexing for reducing space complexicty and processing.
    private void processDictionaryData() {
        HashMap<String, Integer> tempMap = new HashMap<>();
        for (String word : dictionaryData) {
            String key = cleanUpWord(word);
            tempMap.put(key,key.length());
        }
        Set<Map.Entry<String, Integer>> entries = tempMap.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            String word = entry.getKey();
            Integer length = entry.getValue();
            Set<String> stringlengthSet = wordLengthMap.get(length);
            if(stringlengthSet == null) {
                stringlengthSet = new HashSet<>();
                wordLengthMap.put(length,stringlengthSet);
            }
            stringlengthSet.add(word);
        }
        isLoaded = true;
    }

    private String cleanUpWord(String word) {
        word = CodingChallengeUtils.removeSpacesAndPunctuations(word);
        return word.toUpperCase();
    }
}
