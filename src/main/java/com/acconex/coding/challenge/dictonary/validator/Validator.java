package com.acconex.coding.challenge.dictonary.validator;

import com.acconex.coding.challenge.dictonary.exception.DictionaryValidaltionException;

/**
 * Created by rohit on 30/07/16.
 */
public class Validator implements IValidator {


    @Override
    public void validateInputData(String inputLine) throws DictionaryValidaltionException {
        if(containSpaces(inputLine)) {
            throw new DictionaryValidaltionException("Only one word per line is allowed for dictionary.");
        }
    }

    private boolean containSpaces(String line) {
        line  = line.trim();
        return line.split("\\s").length>=2;
    }

}
