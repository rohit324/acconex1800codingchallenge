package com.acconex.coding.challenge.dictonary;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rohit on 25/07/16.
 */
public class DictionaryStoreFactory {

    private static DictionaryStoreFactory instance = new DictionaryStoreFactory();
    private static Map<String, IDictionaryStore> dicStoreMap = new HashMap<>();

    private DictionaryStoreFactory() {

    }

    public static DictionaryStoreFactory getInstance() {
        return instance;
    }

    public IDictionaryStore getDictionaryStore(String dictionaryFilePat) {
        // todo :- move this to util with proper file present checks.
        Path path = Paths.get(dictionaryFilePat);
        path = path.normalize();
        IDictionaryStore iDictionaryStore = getiDictionaryStoreFromPath(path);
        return iDictionaryStore;
    }

    private synchronized IDictionaryStore getiDictionaryStoreFromPath(Path path) {
        IDictionaryStore iDictionaryStore = dicStoreMap.get(path);
        if(iDictionaryStore == null) {
            iDictionaryStore = new DefaultDictionaryStore(path.toString());
        }
        return iDictionaryStore;
    }
}
