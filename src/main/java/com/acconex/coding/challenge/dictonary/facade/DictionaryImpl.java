package com.acconex.coding.challenge.dictonary.facade;

import com.acconex.coding.challenge.dictonary.DictionaryStoreFactory;
import com.acconex.coding.challenge.dictonary.IDictionaryStore;
import com.acconex.coding.challenge.dictonary.exception.DictionaryException;

import java.util.Set;

/**
 * Created by rohit on 24/07/16.
 */
public class DictionaryImpl implements Dictionary {

    IDictionaryStore dictionaryStore;

    private String dictionaryPath;

    public DictionaryImpl(String dictionaryPath) {
        this.dictionaryPath = dictionaryPath;
        dictionaryStore = DictionaryStoreFactory.getInstance().getDictionaryStore(dictionaryPath);
    }

    @Override
    public synchronized void loadDictionary() throws  DictionaryException {
        if (!dictionaryStore.isLoaded()) {
            dictionaryStore.loadDictionary();
        }
    }

    @Override
    public Set<String> getAllWordsOfLength(int length) {
        return dictionaryStore.getAllWordsForLength(length);
    }

    @Override
    public void reloadDictionary() throws DictionaryException {
        loadDictionary();
    }
}
