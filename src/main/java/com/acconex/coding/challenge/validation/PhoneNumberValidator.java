package com.acconex.coding.challenge.validation;

/**
 * Created by rohit on 30/07/16.
 */
public class PhoneNumberValidator {

    private static PhoneNumberValidator instance = new PhoneNumberValidator();
    private static DefaultPhoneNumberValidator validator = new DefaultPhoneNumberValidator();

    private PhoneNumberValidator() {

    }

    public static PhoneNumberValidator getInstance() {
        return instance;
    }

    public IPhoneNumberValidator getValidator() {
        return validator;
    }
}
