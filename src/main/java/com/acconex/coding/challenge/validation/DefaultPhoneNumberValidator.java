package com.acconex.coding.challenge.validation;

import com.acconex.coding.challenge.exception.InputValidationException;

/**
 * Created by rohit on 30/07/16.
 */
public class DefaultPhoneNumberValidator implements IPhoneNumberValidator {

    @Override
    public void validateInputPhoneNumber(String phoneNumber) throws InputValidationException {
        if(phoneNumber.matches(".*[a-zA-Z]+.*")) {
            throw new InputValidationException("Input Phone number :: "+phoneNumber+" must not contain any alphabets");
        }
    }
}
