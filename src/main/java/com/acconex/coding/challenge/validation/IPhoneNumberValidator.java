package com.acconex.coding.challenge.validation;

import com.acconex.coding.challenge.exception.InputValidationException;

/**
 * Created by rohit on 24/07/16.
 */
public interface IPhoneNumberValidator {

    public void validateInputPhoneNumber(String phoneNumber) throws InputValidationException;
}
