package com.acconex.coding.challenge.exception;

/**
 * Created by rohit on 24/07/16.
 */
public class CodingChallengeRuntimeException extends RuntimeException {


    public CodingChallengeRuntimeException(String message) {
        super(message);
    }

}
