package com.acconex.coding.challenge.exception;

/**
 * Created by rohit on 30/07/16.
 */
public class CodingChallengeException extends Exception {

    public CodingChallengeException(String message) {
        super(message);
    }

    public CodingChallengeException(Throwable cause) {
        super(cause);
    }
}
