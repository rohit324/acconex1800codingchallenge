package com.acconex.coding.challenge.exception;

/**
 * Created by rohit on 30/07/16.
 */
public class InputValidationException extends CodingChallengeException{

    public InputValidationException(String message) {
        super(message);
    }

}
