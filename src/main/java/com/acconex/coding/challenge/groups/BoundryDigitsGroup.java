package com.acconex.coding.challenge.groups;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by rohit on 24/07/16.
 */
public class BoundryDigitsGroup extends IDigitGroup {

    private final static Logger logger = Logger.getLogger(BoundryDigitsGroup.class.getName());

    public BoundryDigitsGroup(String groupDigits) {
        super(groupDigits);
    }

    @Override
    public Set<String> getMatchingDictionaryOrBoundryWords() {
        Set<String> wordSet = new HashSet<>();
        // for boundry group all digits form a single group.
        logger.log(Level.FINEST,"Adding " + groupDigits + " to wordSet.");
        wordSet.add(groupDigits);
        return wordSet;
    }
}
