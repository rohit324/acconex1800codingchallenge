package com.acconex.coding.challenge.groups;

import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by rohit on 25/07/16.
 */
public class MixedDigitsGroup extends IDigitGroup {

    private final static Logger logger = Logger.getLogger(MixedDigitsGroup.class.getName());

    public MixedDigitsGroup(String digits) {
        super(digits);
    }

    @Override
    public Set<String> getMatchingDictionaryOrBoundryWords() {
        logger.finer("No matching words are possible for mixed group :- " + groupDigits);
        return null;
    }
}
