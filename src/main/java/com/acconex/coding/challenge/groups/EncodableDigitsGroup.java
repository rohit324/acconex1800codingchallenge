package com.acconex.coding.challenge.groups;

import com.acconex.coding.challenge.dictonary.facade.Dictionary;
import com.acconex.coding.challenge.dictonary.facade.DictionaryManager;
import com.acconex.coding.challenge.groups.matcher.DictionaryMatcher;
import com.acconex.coding.challenge.groups.matcher.IMatcher;

import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by rohit on 24/07/16.
 */
public class EncodableDigitsGroup extends IDigitGroup {

    private final static Logger logger = Logger.getLogger(EncodableDigitsGroup.class.getName());

    public EncodableDigitsGroup(String groupDigits) {
        super(groupDigits);
    }

    @Override
    public Set<String> getMatchingDictionaryOrBoundryWords() {
        Dictionary dictionaryManager = DictionaryManager.getInstance().getDictionary();
        Set<String> dictionaryWordSet = dictionaryManager.getAllWordsOfLength(groupDigits.length());
        IMatcher matcherForGroup = DictionaryMatcher.getInstance().getMatcherForGroup(groupDigits, dictionaryWordSet);
        logger.info("Using matcher class " + matcherForGroup.getClass().getSimpleName());
        return matcherForGroup.createAllWordCombinations();
    }
}
