package com.acconex.coding.challenge.groups;

import com.acconex.coding.challenge.common.Constants;
import com.acconex.coding.challenge.utils.CodingChallengeUtils;

import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by rohit on 24/07/16.
 */
public class DigitsGroupCreator {

    private final static Logger logger = Logger.getLogger(DigitsGroupCreator.class.getName());

    public static IDigitGroup createGroup(String digits) {
        HashSet<Boolean> truthSet = new HashSet<>();
        char[] chars = digits.toCharArray();
        for (char aChar : chars) {
            // the idea is to check if digits contain either one or more or all boundry digits
            truthSet.add(CodingChallengeUtils.isBoundaryDigit(aChar));
        }

        if(truthSet.size() == 2) {
            logger.log(Level.FINE, digits + " contain both boundry digits and words, hence a mixed group.");
            return new MixedDigitsGroup(digits);
        }

        return createBoundryOfWordGroup(digits, truthSet);
    }

    private static IDigitGroup createBoundryOfWordGroup(String digits, HashSet<Boolean> truthSet) {
        if(truthSet.contains(true)) {
            logger.log(Level.FINE, digits + " contain only boundry digits , hence a boundry group.");
            return new BoundryDigitsGroup(digits);
        } else {
            logger.log(Level.FINE, digits + " contain only word digits , hence a word group.");
            return new EncodableDigitsGroup(digits);
        }
    }

    public static boolean containsMixedGroup(String digitsCombination){
        String[] digitGroups = digitsCombination.split(Constants.SEPERATOR);
        for (String groupArray : digitGroups) {
            IDigitGroup group = createGroup(groupArray);
            if (isMixedGroup(digitsCombination, group)) return true;
        }
        logger.log(Level.FINE, "No mixed group found for input " + digitsCombination);
        return false;
    }

    private static boolean isMixedGroup(String digitsCombination, IDigitGroup group) {
        if(group instanceof MixedDigitsGroup) {
            logger.log(Level.FINE, "Found mixed group :- " + group + " for input " + digitsCombination);
            return true;
        }
        return false;
    }
}
