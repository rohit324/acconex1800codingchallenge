package com.acconex.coding.challenge.groups.matcher;

import java.util.Set;

/**
 * Created by rohit on 31/07/16.
 */
public class DictionaryMatcher {

    private static DictionaryMatcher instance = new DictionaryMatcher();

    private DictionaryMatcher(){

    }

    public static DictionaryMatcher getInstance() {
        return instance;
    }

    public IMatcher getMatcherForGroup(String digitsGroup, Set<String> dictionaryWordSet) {
        Set<String> allCombinationsOfWords = MatcherUtils.createAllCombinationsOfWords(digitsGroup);

        if(dictionaryWordSet == null || dictionaryWordSet.size() ==0) {
            return new IMatcher(digitsGroup,dictionaryWordSet);
        }

        if(allCombinationsOfWords.size()<=dictionaryWordSet.size()) {
            return new GroupsTraveserMatcher(digitsGroup,dictionaryWordSet);
        } else {
            return new DictionaryTraveserMatcher(digitsGroup,dictionaryWordSet);
        }
    }

}
