package com.acconex.coding.challenge.groups.matcher;

import com.acconex.coding.challenge.utils.DigitsEncoder;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by rohit on 31/07/16.
 */
public class DictionaryTraveserMatcher extends IMatcher {

    private final static Logger logger = Logger.getLogger(DictionaryTraveserMatcher.class.getName());

    public DictionaryTraveserMatcher(String groupDigits, Set<String> dictionaryWords) {
        super(groupDigits, dictionaryWords);
    }

    @Override
    public Set<String> createAllWordCombinations() {
        Set<String> dictionaryMatchedSet = new LinkedHashSet<>();
        for (String dictionaryWord : getDictionaryWords()) {
            dictionaryWord = dictionaryWord.toUpperCase();
            char[] dictionaryWordCharArray = dictionaryWord.toCharArray();
            int index=0;
            index = findCompleteMatchForDictionaryWord(dictionaryWordCharArray, index);
            if(index == dictionaryWordCharArray.length) {
                logger.log(Level.FINE,"Found match for the word " + dictionaryWord);
                dictionaryMatchedSet.add(dictionaryWord);
            } else {
                logger.log(Level.FINE,"No dictionary match found for the word  " + getGroupDigits());
                handleNoMatchingWords(dictionaryMatchedSet);
            }
        }
        return dictionaryMatchedSet;
    }

    private int findCompleteMatchForDictionaryWord(char[] dictionaryWordCharArray, int index) {
        char[] encodableWordChars = getGroupDigits().toCharArray();
        // iterate sequenctially over every char in dictionary word and match it against
        // all alphabet combinations for a digitgroup sequentially .
        for (;index<dictionaryWordCharArray.length;index++) {
            Set<String> encodedAlphabetsForADigitSet = DigitsEncoder.
                    getEncodedAlphabetsForADigit(encodableWordChars[index]);
            if(encodedAlphabetsForADigitSet.contains(dictionaryWordCharArray[index]+"")){
                continue;
            } else {
                break;
            }
        }
        return index;
    }
}
