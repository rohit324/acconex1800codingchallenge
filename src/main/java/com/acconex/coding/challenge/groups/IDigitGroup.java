package com.acconex.coding.challenge.groups;

import java.util.Set;

/**
 * Created by rohit on 24/07/16.
 */
public abstract class IDigitGroup {

    protected String groupDigits;

    public IDigitGroup(String groupDigits) {
        this.groupDigits = groupDigits;
    }

    public int getLength() {
        return groupDigits.length();
    }

    abstract public Set<String> getMatchingDictionaryOrBoundryWords();
}
