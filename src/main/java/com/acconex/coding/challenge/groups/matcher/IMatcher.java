package com.acconex.coding.challenge.groups.matcher;


import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by rohit on 31/07/16.
 */
public class IMatcher {

    private String groupDigits;

    private Set<String> dictionaryWords;

    public IMatcher(String groupDigits, Set<String> dictionaryWords) {
        this.groupDigits = groupDigits;
        this.dictionaryWords = dictionaryWords;
    }

    public String getGroupDigits() {
        return groupDigits;
    }

    public Set<String> getDictionaryWords() {
        return dictionaryWords;
    }

    public Set<String> createAllWordCombinations() {
        LinkedHashSet<String> map = new LinkedHashSet<>();
        handleNoMatchingWords(map);
        return map;
    }

    public void handleNoMatchingWords(Set<String> dictionaryMatchedSet) {
        if(groupDigits.length() ==1) {
            dictionaryMatchedSet.add(groupDigits);
        }
    }

}
