package com.acconex.coding.challenge.groups.matcher;

import com.acconex.coding.challenge.processor.ConvertorUtil;
import com.acconex.coding.challenge.utils.DigitsEncoder;

import java.util.LinkedList;
import java.util.Set;

/**
 * Created by rohit on 31/07/16.
 */
public class MatcherUtils {

    public static Set<String> createAllCombinationsOfWords(String groupDigits) {
        LinkedList<Set<String>> alphbetsSets = new LinkedList<>();
        char[] chars = groupDigits.toCharArray();
        for (char aChar : chars) {
            Set<String> encodedAlphabetsForADigit = DigitsEncoder.getEncodedAlphabetsForADigit(aChar);
            alphbetsSets.add(encodedAlphabetsForADigit);
        }
        return ConvertorUtil.createCombinationForWordSets(alphbetsSets);
    }

}
