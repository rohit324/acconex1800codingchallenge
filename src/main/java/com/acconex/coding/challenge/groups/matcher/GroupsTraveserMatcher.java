package com.acconex.coding.challenge.groups.matcher;

import com.acconex.coding.challenge.common.Constants;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by rohit on 31/07/16.
 */
public class GroupsTraveserMatcher extends IMatcher {


    public GroupsTraveserMatcher(String groupDigits, Set<String> dictionaryWords) {
        super(groupDigits, dictionaryWords);
    }

    @Override
    public Set<String> createAllWordCombinations() {
        Set<String> dictionaryMatchedSet = new LinkedHashSet<>();
        for (String combinationForWordSet : MatcherUtils.createAllCombinationsOfWords(getGroupDigits())) {
            String word = combinationForWordSet.replaceAll(Constants.SEPERATOR, "");
            if(getDictionaryWords().contains(word)) {
                dictionaryMatchedSet.add(word);
            }
        }
        return dictionaryMatchedSet;
    }
}
