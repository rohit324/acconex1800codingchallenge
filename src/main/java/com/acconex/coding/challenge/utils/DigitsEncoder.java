package com.acconex.coding.challenge.utils;

import com.acconex.coding.challenge.exception.CodingChallengeRuntimeException;

import java.util.*;

/**
 * Created by rohit on 24/07/16.
 */


public class DigitsEncoder {

    private static Map<String, String> digitMap = new HashMap<>();

    // TODO :- adding flexibility for changing the map at runtime, by reading from prop files.
    static {
        digitMap.put("2", "A,B,C");
        digitMap.put("3", "D,E,F");
        digitMap.put("4", "G,H,I");
        digitMap.put("5", "J,K,L");
        digitMap.put("6", "M,N,O");
        digitMap.put("7", "P,Q,R,S");
        digitMap.put("8", "T,U,V");
        digitMap.put("9", "W,X,Y,Z");
    }

    public static Map<String,String> getDigitMap() {
        return new HashMap<>(digitMap);
    }

    public static Set<String> getEncodedAlphabetsForADigit(char digit) {
        // todo :- optimize set creation everytime.
        String encodedAlphas = digitMap.get("" + digit);
        if (encodedAlphas == null) {
            throw new CodingChallengeRuntimeException("Not able to find the encoding for digit" + digit);
        }

        String[] split = encodedAlphas.split(",");
        return new LinkedHashSet<>(Arrays.asList(split));
    }

}
