package com.acconex.coding.challenge.utils;

import com.acconex.coding.challenge.exception.CodingChallengeRuntimeException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rohit on 24/07/16.
 */
public class CodingChallengeUtils {

    // Todo :- It will be good to provide flexibity for setting changing the boundry words.
    private static List<String> boundaryWordSet = new ArrayList<>();

    static {
        boundaryWordSet.add("0");
        boundaryWordSet.add("1");
    }

    public static String removeSpacesAndPunctuations(String input)  {
        // TODO :- Lets try and use not null annotation.
        if(input == null) {
            throw new CodingChallengeRuntimeException("Input is mandatory and cannot be null");
        }
        return input.replaceAll("[\\W]", "");
    }

    public static boolean containsAlphabets(String input) {
        return !input.equals(input.replaceAll("[a-zA-Z]", ""));
    }

    public static boolean isBoundaryDigit(char letter) {
        return boundaryWordSet.contains(""+letter);
    }

    public static String getBoundryCharactor(int position){
        return boundaryWordSet.get(position);
    }
}
