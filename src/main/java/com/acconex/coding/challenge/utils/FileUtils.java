package com.acconex.coding.challenge.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

/**
 * Created by rohit on 28/07/16.
 */
public class FileUtils {

    public static String getAbsolutePath(String path) throws FileNotFoundException {
        String message = "No File found at the location " + path;
        File file = new File(path);
        if(file.exists()) {
            return file.getAbsolutePath();
        }
        URL resource = FileUtils.class.getClassLoader().getResource(path);
        if(resource ==null) {
            throw new FileNotFoundException(message);
        }
        return resource.getPath();
    }

}
