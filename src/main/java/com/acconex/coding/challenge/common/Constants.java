package com.acconex.coding.challenge.common;

/**
 * Created by rohit on 24/07/16.
 */
public interface Constants {

    String SEPERATOR = "-";
    String LOGGING_PROPERTIES_FILE = "logging.properties";
}
