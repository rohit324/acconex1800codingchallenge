package com.acconex.coding.challenge.processor;

import com.acconex.coding.challenge.common.Constants;
import com.acconex.coding.challenge.groups.DigitsGroupCreator;
import com.acconex.coding.challenge.groups.IDigitGroup;
import com.acconex.coding.challenge.utils.CodingChallengeUtils;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by rohit on 25/07/16.
 */
public class ConvertorUtil {

    private final static Logger logger = Logger.getLogger(ConvertorUtil.class.getName());

    public static Set<String> createCombinationForWordSets(Collection<Set<String>> wordSets) {
        if(wordSets == null || wordSets.size() ==0) {
            logger.info("Returning empty set for input " + wordSets);
            return new LinkedHashSet<>();
        }

        if(wordSets.size() == 1) {
            logger.info("No processing required is input has just 1 word, " +
                    "hence returning single word set. " + wordSets);
            return  new LinkedHashSet<>(wordSets.iterator().next());
        }
        Iterator<Set<String>> iterator = wordSets.iterator();
        Set<String> firstSet = iterator.next();
        while(iterator.hasNext()) {
            Set<String> mergeOf2Sets = createCombinationsFor2Sets(firstSet, iterator.next());
            firstSet = mergeOf2Sets;
        }
        return firstSet;
    }

    public static Set<String> createCombinationsFor2Sets(Set<String> set1, Set<String> set2){
        logger.fine("merging sets :- set 1 = " + set1 +" and set2 = " + set2);
        HashSet<String> combinedSet = new LinkedHashSet<>();
        if(set1 == null || set1.size() ==0 || set2 == null || set2.size() ==0) {
            combinedSet.addAll(set1);
            combinedSet.addAll(set2);
        }
        for (String word : set1) {
            for (String word2 : set2) {
                StringBuffer buffer = new StringBuffer();
                buffer.append(word+Constants.SEPERATOR+word2);
                combinedSet.add(buffer.toString());
            }
        }
        logger.fine("Returning merged set  = " + combinedSet);
        return combinedSet;
    }



    public static boolean isNonBoundryDigitType(String group) {
        char firstChar = group.charAt(0);
        boolean isDigit = Character.isDigit(firstChar);
        boolean boundaryDigit = CodingChallengeUtils.isBoundaryDigit(firstChar);
        if(boundaryDigit) return false;
        return isDigit;
    }

    public static Map<Integer,Set<String>> generateDictionarySetsForEveryDigitalGroup(String digitsCombination) {
        // Only those groups are valid which are either have all boundry digits or all encoded digits.
        // combination containing the both boundry digits and encoded digits.
        if(DigitsGroupCreator.containsMixedGroup(digitsCombination)) {
            logger.fine("No valid sets can be generated containing mixed groups for input " + digitsCombination);
            return null;
        }
        String[] digitGroups = digitsCombination.split(Constants.SEPERATOR);
        Map<Integer, Set<String>> indexMatchingWordsMap = createMatchingWordsMapFromDictionary(digitsCombination, digitGroups);
        if(indexMatchingWordsMap.size()==digitGroups.length) {
            return indexMatchingWordsMap;
        } else {
            logger.fine("Match could not be found for all groups for the combination " + digitsCombination);
            return null;
        }
    }

    private static Map<Integer, Set<String>> createMatchingWordsMapFromDictionary(String digitsCombination, String[] digitGroups) {
        Map<Integer,Set<String>> indexMatchingWordsMap = new LinkedHashMap<>();
        for (int i=0;i<digitGroups.length;i++) {
            IDigitGroup group = DigitsGroupCreator.createGroup(digitGroups[i]);
            Set<String> matchingDictionaryWords = group.getMatchingDictionaryOrBoundryWords();
            if(matchingDictionaryWords.size()>0) {
                indexMatchingWordsMap.put(i,matchingDictionaryWords);
            } else {
                // it is mandatory to find match for every group to generate a correct word combination.
                logger.fine("Valid Sets can only contain boundry and word group, input set  " + digitsCombination +
                        " is not valid");
                return new LinkedHashMap<>();
            }
        }
        return indexMatchingWordsMap;
    }
}
