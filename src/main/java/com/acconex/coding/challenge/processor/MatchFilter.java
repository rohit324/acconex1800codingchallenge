package com.acconex.coding.challenge.processor;

import com.acconex.coding.challenge.common.Constants;
import com.acconex.coding.challenge.utils.CodingChallengeUtils;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by rohit on 01/08/16.
 */
public class MatchFilter {

    private final static Logger logger = Logger.getLogger(MatchFilter.class.getName());

    private Set<String> potentialMatchSet = new LinkedHashSet<>();

    public MatchFilter(Set<String> potentialMatchSet) {
        this.potentialMatchSet = potentialMatchSet;
    }

    public Set<String> filterResults() {
        potentialMatchSet = removeMatchesContainingOnlyBoundryGroups();
        return removeConsecutiveNotMatchGroups();
    }

    public  Set<String > removeMatchesContainingOnlyBoundryGroups() {
        logger.fine(" Removing boundry groups from the input set " + potentialMatchSet);
        HashSet<String> cleanedSet = new HashSet<>();
        for (String match : potentialMatchSet) {
            boolean alphabetPresent = CodingChallengeUtils.containsAlphabets(match);
            if(alphabetPresent) {
                cleanedSet.add(match);
            } else {
                logger.fine(" No words in potential phone number, skipping match::" + match);
            }
        }
        logger.fine(" Cleaned Set after removing boundry matches  " + cleanedSet);
        return cleanedSet;
    }

    public  Set<String > removeConsecutiveNotMatchGroups() {
        logger.fine(" Removing consecutive matches with digits " + potentialMatchSet);
        HashSet<String> cleanedSet = new HashSet<>();
        for (String match : potentialMatchSet) {
            String[] groups = match.split(Constants.SEPERATOR);
            if(groups.length<2) cleanedSet.add(match);

            if (doesMatchContainsConsecutiveDigits(groups)) {
                logger.fine(" Skipping the match::" + match + " as it contains consecutive digits which could not be encoded.");
            } else {
                cleanedSet.add(match);
            }
        }
        logger.fine(" Cleaned Set after removing boundry matches  " + cleanedSet);
        return cleanedSet;
    }

    private boolean doesMatchContainsConsecutiveDigits(String[] groups) {
        for(int i=1;i<groups.length;i++) {
            String group1 = groups[i - 1];
            String group2 = groups[i];
            if(group1.length() == 1 && group2.length() == 1) {
                if(ConvertorUtil.isNonBoundryDigitType(group1) && ConvertorUtil.isNonBoundryDigitType(group2)) {
                    return true;
                }
            }
        }
        return false;
    }
}
