package com.acconex.coding.challenge.processor;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by rohit on 24/07/16.
 */
public class DefaultDigitalConvertor implements IDigitalNumberToWordConvertor {

    private final static Logger logger = Logger.getLogger(DefaultDigitalConvertor.class.getName());

    String inputDigitalNumber;
    Set<String> matchedSet = new LinkedHashSet<>();
    boolean isConverted;

    public DefaultDigitalConvertor(String inputDigitalNumber) {
        this.inputDigitalNumber = inputDigitalNumber;
    }

    @Override
    public Set<String> getAllMatchingNumbers() {
        if(!isConverted) convert();
        return matchedSet;
    }

    @Override
    public boolean matchFound() {
        if(!isConverted) convert();
        return getAllMatchingNumbers().size()>0;
    }

    public String generateFormattedOutput() {
        //TODO :- see if we need to create format handler for more flexibility
        final StringBuffer stringBuffer = new StringBuffer();
        if(matchedSet.size() > 0) {
            for (String match : matchedSet) {
                stringBuffer.append(match+"\n");
            }
        }
        return stringBuffer.toString();
    }

    private void convert() {
        logger.log(Level.INFO,"Started conversion for number " + inputDigitalNumber);
        Set<String> allDigitsCombinations = getAllCombinationsOfDigits();
        logger.log(Level.INFO," Potential Matches :- \n " + allDigitsCombinations);
        convertDigitalToWordMatches(allDigitsCombinations);
        logger.log(Level.INFO," All Matches including only boundry group words :- \n " + matchedSet);
        matchedSet = new MatchFilter(matchedSet).filterResults();
        isConverted = true;
    }

    private void convertDigitalToWordMatches(Set<String> allDigitsCombinations) {
        for (String digitsCombination : allDigitsCombinations) {
            Map<Integer, Set<String>> indexMatchingWordsMap = ConvertorUtil.generateDictionarySetsForEveryDigitalGroup(digitsCombination);
            getAllEncodedPhoneNumbersFrmWordGroups(digitsCombination, indexMatchingWordsMap);
        }
    }

    private void getAllEncodedPhoneNumbersFrmWordGroups(String digitsCombination, Map<Integer, Set<String>> indexMatchingWordsMap) {
        if(indexMatchingWordsMap!=null && indexMatchingWordsMap.size()>0) {
            Collection<Set<String>> values = indexMatchingWordsMap.values();
            logger.log(Level.INFO," Match found for the combination :- " + digitsCombination +
                    " matches are " + values);
            matchedSet.addAll(ConvertorUtil.createCombinationForWordSets(values));
            logger.log(Level.INFO," Matched Set details after  :- " + digitsCombination + " total matches " + matchedSet);
        }else {
            logger.log(Level.INFO," No Dictionary match found for the combination :- " + digitsCombination);
        }
    }

    private Set<String> getAllCombinationsOfDigits() {
        DigitsCombinationGenerator generator = new DigitsCombinationGenerator(inputDigitalNumber);
        return generator.getAllCombinationOfDigits();
    }

}
