package com.acconex.coding.challenge.processor;

import java.util.Set;

/**
 * Created by rohit on 24/07/16.
 */
public interface IDigitalNumberToWordConvertor {

    // TODO:- Lets try and find a better name for the method.
    Set<String> getAllMatchingNumbers();

    boolean matchFound();

    public String generateFormattedOutput();

}
