package com.acconex.coding.challenge.processor;

/**
 * Created by rohit on 24/07/16.
 */
public class PhoneNumberConvertorFactory {

    private static PhoneNumberConvertorFactory instance =  new PhoneNumberConvertorFactory();

    private PhoneNumberConvertorFactory(){

    }

    public static PhoneNumberConvertorFactory getInstance(){
        return instance;
    }

    public IDigitalNumberToWordConvertor getPhoneNumberConvertor(String phoneNumber) {
        return new DefaultDigitalConvertor(phoneNumber);
    }
}
