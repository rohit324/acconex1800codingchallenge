package com.acconex.coding.challenge.processor;

import com.acconex.coding.challenge.common.Constants;
import com.acconex.coding.challenge.utils.CodingChallengeUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.acconex.coding.challenge.utils.CodingChallengeUtils.isBoundaryDigit;

/**
 * Created by rohit on 24/07/16.
 */
public class DigitsCombinationGenerator {

    private String digits;
    private Set<String> combinations = new HashSet<String>();
    private Map<String,Set<String>> digitCombinationCache = new HashMap<>();

    public DigitsCombinationGenerator(String digits) {
        this.digits = digits;
    }

    public Set<String> getAllCombinationOfDigits(){
        HashSet<String> combinationSet = new HashSet<>();
        if(digits == null || digits.length() ==0) {
            return combinationSet;
        }
        char[] chars = digits.toCharArray();
        if(chars.length<=1){
            return generateCombinations(digits);
        }

        for(int i=1;i<chars.length;i++) {
            String firstPart = digits.substring(0,i);
            String secondPart = digits.substring(i);
            char firstPartEnd = firstPart.charAt(firstPart.length() - 1);
            char secondPartStart = secondPart.charAt(0);
            // first part end and second part start are boundry digits, lets just move forward
            // to the next digit to create combinations.
            if(isBoundaryDigit(firstPartEnd) && isBoundaryDigit(secondPartStart)) {
                continue;
            }
            Set<String> firstHalfCombinations = generateCombinations(firstPart);
            Set<String> secondHalfCombinations = generateCombinations(secondPart);
            for (String firstHalfCombination : firstHalfCombinations) {
                for (String secondHalfCombination : secondHalfCombinations) {
                    String result = concatenateString(firstHalfCombination, secondHalfCombination);
                    combinationSet.add(result);
                }
            }
        }
        combinationSet.add(digits);
        return combinationSet;
    }

    private Set<String> generateCombinations(String input) {
        HashSet<String> combinationSet = new HashSet<>();
        combinationSet.add(input);
        if(input.length() ==1) {
            return combinationSet;
        }

        char[] chars = input.toCharArray();
        int boundry = getBoundryIndex(chars);

        if (boundry == chars.length) {
            return combinationSet;
        }

        if(boundry ==0) {
            boundry++;
        }
        String firstPart = input.substring(0,boundry);
        String secondPart = input.substring(boundry);
        Set<String> secondPartSet = generateCombinations(secondPart);
        for (String string : secondPartSet) {
            combinationSet.add(concatenateString(firstPart, string));
        }
        return combinationSet;
    }


    private String concatenateString(String firstPart, String string) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(firstPart);
        stringBuilder.append(Constants.SEPERATOR);
        stringBuilder.append(string);
        return stringBuilder.toString();
    }

    private int getBoundryIndex(char[] chars) {
        int i = 0;
        for (; i < chars.length; i++) {
            if(CodingChallengeUtils.isBoundaryDigit(chars[i])) {
                continue;
            } else {
                break;
            }
        }
        return i;
    }
}
