package com.acconex.coding.challenge.services.CLI.handlers;

import com.acconex.coding.challenge.services.CLI.exception.CommandException;
import com.acconex.coding.challenge.services.CLI.utils.HandlerUtil;
import static com.acconex.coding.challenge.services.CLI.CommandConstants.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by rohit on 30/07/16.
 */
public class NoOptionHandler{

    public void handleInputCommand(LinkedList<String> argList) throws CommandException {
        if(argList == null || argList.size() ==0) {
            return;
        }
        processAllPhoneNumbers(argList);
    }

    private void processAllPhoneNumbers(List argList) throws CommandException {
        for (Object  arg : argList) {
            System.out.println(PROCESSING + arg.toString());
            HandlerUtil.processInputPhoneNumber(arg.toString());
        }
    }
}
