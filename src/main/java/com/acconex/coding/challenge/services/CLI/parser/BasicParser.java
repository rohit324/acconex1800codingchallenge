package com.acconex.coding.challenge.services.CLI.parser;

import com.acconex.coding.challenge.services.CLI.exception.ParserException;

/**
 * Created by rohit on 01/08/16.
 */
public class BasicParser implements CommandLineParser{

    @Override
    public CommandLine parse(Options options, String[] args) throws ParserException {
        CommandLine commandLine = new CommandLine();
        for (int i=0;i<args.length;) {
            if(args[i].startsWith("-")) {
                addOption(options, args, commandLine, i);
                i =i+2;
            }
            else
            {
                commandLine.addArgs(args[i]);
                i++;
            }
        }
        return commandLine;
    }

    private void addOption(Options options, String[] args, CommandLine commandLine, int i) throws ParserException {
        Option validOption = options.isValidOption(args[i]);
        if(validOption!=null) {
            checkDoesArgExists(args,i+1,validOption.getOptionValue());
            validOption.setArg(args[i+1]);
            commandLine.addOption(validOption);
        } else {
            throw new ParserException("Arg::" + args[i] + " is not supported.");
        }
    }

    private void checkDoesArgExists(String[] args, int index, String option) throws ParserException {
        if(args.length<index+1) {
            throw new ParserException("Argument value is mandatory for option " + option);
        }
    }
}
