package com.acconex.coding.challenge.services.CLI.parser;

import java.util.LinkedList;

/**
 * Created by rohit on 01/08/16.
 */
public class CommandLine {

    private LinkedList<Option> options = new LinkedList<>();
    private LinkedList<String> args = new LinkedList<>();

    public void addOption(Option option){
        options.add(option);
    }

    public void addArgs(String arg) {
        args.add(arg);
    }

    public LinkedList<Option> getOptions() {
        return options;
    }

    public LinkedList<String> getArgs() {
        return args;
    }
}
