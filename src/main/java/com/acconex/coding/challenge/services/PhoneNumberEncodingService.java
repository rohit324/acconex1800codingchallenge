package com.acconex.coding.challenge.services;

import com.acconex.coding.challenge.common.Constants;
import com.acconex.coding.challenge.exception.InputValidationException;
import com.acconex.coding.challenge.processor.IDigitalNumberToWordConvertor;
import com.acconex.coding.challenge.processor.PhoneNumberConvertorFactory;
import com.acconex.coding.challenge.utils.CodingChallengeUtils;
import com.acconex.coding.challenge.utils.FileUtils;
import com.acconex.coding.challenge.validation.IPhoneNumberValidator;
import com.acconex.coding.challenge.validation.PhoneNumberValidator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Set;

/**
 * Created by rohit on 25/07/16.
 */
public class PhoneNumberEncodingService {

    String phoneNumber;
    IDigitalNumberToWordConvertor convertor;


    // setup logging before we enter the module.
    static {
        setUpLogging();
    }

    public PhoneNumberEncodingService(String phoneNumber) throws InputValidationException {
        this.phoneNumber = phoneNumber;
        phoneNumber = CodingChallengeUtils.removeSpacesAndPunctuations(phoneNumber);
        validateInputNumber(phoneNumber);
        convertor = PhoneNumberConvertorFactory.getInstance().getPhoneNumberConvertor(phoneNumber);
    }

    private void validateInputNumber(String phoneNumber) throws InputValidationException {
        IPhoneNumberValidator validator = PhoneNumberValidator.getInstance().getValidator();
        validator.validateInputPhoneNumber(phoneNumber);
    }

    public String getEncodedPhoneNumbers() {
        if(!convertor.matchFound() ) {
            return null;
        } else {
            return convertor.generateFormattedOutput();
        }
    }

    public Set<String> getAllMatches() {
        return convertor.getAllMatchingNumbers();
    }

    private static void setUpLogging() {
        String absolutePath = null;
        try {
            absolutePath = FileUtils.getAbsolutePath(Constants.LOGGING_PROPERTIES_FILE);
        } catch (FileNotFoundException e) {
            System.out.println("\n\n Not able to find logging.properties file, everything will be logged on console.");
        }
        File file = new File(absolutePath);
        System.setProperty("java.util.logging.config.file", file.getAbsolutePath());
    }
}
