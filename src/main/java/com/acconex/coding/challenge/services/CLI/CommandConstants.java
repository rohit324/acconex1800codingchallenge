package com.acconex.coding.challenge.services.CLI;

/**
 * Created by rohit on 30/07/16.
 */
public interface CommandConstants {

    // error messages
    String ERROR_DIC_PATH_MANDATORY = "Dictionary Path is mandatory.";
    String DIC_LOAD_ERROR = "Error loading dictionary , Error Details :- ";
    String ERROR_DIC_FILE_NOT_FOUND = "Not able to find dictionary file at ";
    String ERROR_MISSING_INPUT_FILE_PARAM = "Input File Path is mandatory.";
    String ERROR_INPUT_FILE_NOT_FOUND = "Not able to find file at path::";


    // log messages
    String PROCESSING = "Processing the Phone number::";
    String NO_MATCH = "**** No Match Found for the Number::";
    String HELP_MSG = "Enter the Digital Phone Number.";
    String MATCHES = "Matches for number::";
    String START = "======== Start ==========";
    String END = "======== End ============";

}
