package com.acconex.coding.challenge.services.CLI.exception;

import com.acconex.coding.challenge.exception.CodingChallengeException;

/**
 * Created by rohit on 30/07/16.
 */
public class CommandException extends CodingChallengeException{

    public CommandException(Throwable cause) {
        super(cause);
    }

    public CommandException(String message) {
        super(message);
    }
}
