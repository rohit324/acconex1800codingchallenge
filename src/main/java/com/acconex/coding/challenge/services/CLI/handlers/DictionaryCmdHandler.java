package com.acconex.coding.challenge.services.CLI.handlers;

import com.acconex.coding.challenge.dictonary.exception.DictionaryException;
import com.acconex.coding.challenge.dictonary.facade.DictionaryManager;
import com.acconex.coding.challenge.services.CLI.exception.CommandException;
import com.acconex.coding.challenge.services.CLI.parser.Option;
import com.acconex.coding.challenge.utils.FileUtils;
import static com.acconex.coding.challenge.services.CLI.CommandConstants.*;

import java.io.FileNotFoundException;

/**
 * Created by rohit on 30/07/16.
 */
public class DictionaryCmdHandler implements  ICommandHandler {

    @Override
    public void handleInputCommand(Option option) throws CommandException {
        String optionValue = option.getArg();
        if(optionValue == null || optionValue.isEmpty()) {
            System.out.println(ERROR_DIC_PATH_MANDATORY);
            return;
        }
        String dictionaryPath = getFilePath(optionValue);
        loadNewDictionary(dictionaryPath);
    }


    private void loadNewDictionary(String dictionaryPath) throws CommandException {
        try {
            DictionaryManager.getInstance().loadNewDictionary(dictionaryPath);
        } catch (DictionaryException e) {
            System.out.println(DIC_LOAD_ERROR + e.getMessage());
            throw new CommandException(e);
        }
    }

    private String getFilePath(String path) throws CommandException {
        String dictionaryPath;
        try {
            dictionaryPath = FileUtils.getAbsolutePath(path);
        } catch (FileNotFoundException e) {
            System.out.println(ERROR_DIC_FILE_NOT_FOUND + path);
            throw new CommandException(e);
        }
        return dictionaryPath;
    }
}
