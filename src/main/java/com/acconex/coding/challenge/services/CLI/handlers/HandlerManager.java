package com.acconex.coding.challenge.services.CLI.handlers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rohit on 30/07/16.
 */
public class HandlerManager {

    private static Map<String,ICommandHandler> handlerMap = new HashMap<>();
    private static NoOptionHandler noOptHandlerInstance = new NoOptionHandler();

    private static HandlerManager instance = new HandlerManager();

    static {
        handlerMap.put("-i", new InputFileCmdHandler());
        handlerMap.put("-d", new DictionaryCmdHandler());
    }

    public static HandlerManager getInstance() {
        return instance;
    }

    public ICommandHandler getHandler(String option) {
        ICommandHandler iCommandHandler = handlerMap.get(option);
        return iCommandHandler;
    }

    public NoOptionHandler getNoOptHandler() {
        return noOptHandlerInstance;
    }
}
