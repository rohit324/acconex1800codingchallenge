package com.acconex.coding.challenge.services.CLI.parser;

/**
 * Created by rohit on 01/08/16.
 */
public class Option {

    private String optionValue;
    private String description;
    private String arg;

    public Option(String optionValue, String description) {
        this.optionValue = optionValue;
        this.description = description;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }
}
