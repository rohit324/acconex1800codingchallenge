package com.acconex.coding.challenge.services.CLI.utils;

import com.acconex.coding.challenge.exception.InputValidationException;
import com.acconex.coding.challenge.services.CLI.exception.CommandException;
import com.acconex.coding.challenge.services.PhoneNumberEncodingService;
import static com.acconex.coding.challenge.services.CLI.CommandConstants.*;

/**
 * Created by rohit on 30/07/16.
 */
public class HandlerUtil {

    public static void processInputPhoneNumber(String phoneNumber) throws CommandException {
        try {
            PhoneNumberEncodingService service = new PhoneNumberEncodingService(phoneNumber);
            String encodedPhoneNumbers = service.getEncodedPhoneNumbers();
            if(encodedPhoneNumbers == null) {
                printNullresult(phoneNumber);
            } else {
                printFormattedResult(encodedPhoneNumbers,phoneNumber);
            }
        } catch (InputValidationException e) {
            throw new CommandException(e);
        }
    }

    private static void printFormattedResult(String encodedPhoneNumbers, String phoneNumber) {
        System.out.println(MATCHES + phoneNumber);
        System.out.println(START);
        System.out.println(encodedPhoneNumbers);
        System.out.println(END);
    }

    private static void printNullresult(String phoneNumber) {
        System.out.println(NO_MATCH + phoneNumber);
    }
}
