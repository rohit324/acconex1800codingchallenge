package com.acconex.coding.challenge.services.CLI.handlers;

import com.acconex.coding.challenge.services.CLI.exception.CommandException;
import com.acconex.coding.challenge.services.CLI.parser.Option;

/**
 * Created by rohit on 30/07/16.
 */
public interface ICommandHandler {

    void handleInputCommand(Option option) throws CommandException;
}
