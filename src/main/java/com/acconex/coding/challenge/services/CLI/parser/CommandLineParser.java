package com.acconex.coding.challenge.services.CLI.parser;

import com.acconex.coding.challenge.services.CLI.exception.ParserException;

/**
 * Created by rohit on 01/08/16.
 */
public interface CommandLineParser {

    CommandLine parse(Options options, String[] args) throws ParserException;
}
