package com.acconex.coding.challenge.services.CLI;

import com.acconex.coding.challenge.services.CLI.exception.CommandException;
import com.acconex.coding.challenge.services.CLI.exception.ParserException;
import com.acconex.coding.challenge.services.CLI.handlers.HandlerManager;
import com.acconex.coding.challenge.services.CLI.handlers.ICommandHandler;
import com.acconex.coding.challenge.services.CLI.handlers.NoOptionHandler;
import com.acconex.coding.challenge.services.CLI.parser.*;

import java.util.LinkedList;

/**
 * Created by rohit on 30/07/16.
 */
public class CLIService {

    static HandlerManager handlerManager = HandlerManager.getInstance();


    public static void main(String[] args) throws Exception {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = getCommandLine(args, parser);

        LinkedList<Option> options = cmd.getOptions();

        for (Option option : options) {
            ICommandHandler handler = handlerManager.getHandler(option.getOptionValue());
            handler.handleInputCommand(option);
        }

        // when on options then just print encoded number
        handleNoInputOptions(cmd);
    }

    private static void handleNoInputOptions(CommandLine cmd) throws CommandException {
        NoOptionHandler noOptHandler = handlerManager.getNoOptHandler();
        noOptHandler.handleInputCommand(cmd.getArgs());
    }

    private static CommandLine getCommandLine(String[] args, CommandLineParser parser) throws ParserException {
        Options options = getSetupOptions();
        return parser.parse( options, args);
    }

    private static Options getSetupOptions() {
        Options options = new Options();
        options.addOption(new Option("-d","Load Dictionary."));
        options.addOption(new Option("-i","Input File With Phone Numbers."));
        return options;
    }

}
