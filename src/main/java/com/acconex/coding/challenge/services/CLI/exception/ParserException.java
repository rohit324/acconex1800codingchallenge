package com.acconex.coding.challenge.services.CLI.exception;

/**
 * Created by rohit on 01/08/16.
 */
public class ParserException  extends CommandException{

    public ParserException(String message) {
        super(message);
    }
}
