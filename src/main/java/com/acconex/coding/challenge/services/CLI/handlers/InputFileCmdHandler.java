package com.acconex.coding.challenge.services.CLI.handlers;

import com.acconex.coding.challenge.services.CLI.exception.CommandException;
import com.acconex.coding.challenge.services.CLI.parser.Option;
import com.acconex.coding.challenge.services.CLI.utils.HandlerUtil;
import com.acconex.coding.challenge.utils.FileUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import static com.acconex.coding.challenge.services.CLI.CommandConstants.*;

/**
 * Created by rohit on 30/07/16.
 */
public class InputFileCmdHandler implements ICommandHandler{

    @Override
    public void handleInputCommand(Option option) throws CommandException {
        String optionValue = option.getArg();
        if(optionValue == null || optionValue.isEmpty()) {
            throw new CommandException(ERROR_MISSING_INPUT_FILE_PARAM);
        }
        if (getAbsolutePath(optionValue)) return;

        processFile(optionValue);
    }


    private boolean getAbsolutePath(String filePath) throws CommandException {
        try {
            FileUtils.getAbsolutePath(filePath);
        } catch (FileNotFoundException e) {
            System.out.println(ERROR_INPUT_FILE_NOT_FOUND + filePath);
            throw new CommandException(ERROR_INPUT_FILE_NOT_FOUND + filePath);
        }
        return false;
    }

    private void processFile(String filePath) throws CommandException {
        try {
            List<String> list = Files.readAllLines(Paths.get(filePath));
            for (String phoneNumber : list) {
                HandlerUtil.processInputPhoneNumber(phoneNumber);
            }
        } catch (IOException e) {
            throw new CommandException(e);
        }
    }
}
