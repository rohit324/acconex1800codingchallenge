package com.acconex.coding.challenge.services.CLI.parser;

import java.util.LinkedList;

/**
 * Created by rohit on 01/08/16.
 */
public class Options {

    private LinkedList<Option> optionList = new LinkedList<>();

    public void addOption(Option option) {
        optionList.add(option);
    }

    public Option isValidOption(String shortArg) {
        for (Option option : optionList) {
            if(shortArg.equalsIgnoreCase(option.getOptionValue())) {
                return option;
            }
        }
        return null;
    }
}
