package com.acconex.coding.challenge.processor;

import com.acconex.coding.challenge.common.BaseTest;
import com.acconex.coding.challenge.harnes.builders.TestMixedPhoneNumberBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by rohit on 27/07/16.
 */
public class DigitalConvertorTest extends BaseTest {

    private final static Logger logger = Logger.getLogger(DigitalConvertorTest.class.getName());


    @Test
    public void testEncodingForWordsLength2() {
        TestMixedPhoneNumberBuilder builder = new TestMixedPhoneNumberBuilder(3, 2);
        findCombinations(builder);
    }

    @Test
    public void testEncodingForWordsLength3() {
        TestMixedPhoneNumberBuilder builder = new TestMixedPhoneNumberBuilder(3, 3);
        findCombinations(builder);
    }

    @Test
    public void testEncodingForWordsLength4() {
        TestMixedPhoneNumberBuilder builder = new TestMixedPhoneNumberBuilder(3, 4);
        findCombinations(builder);
    }

    @Test
    public void testEncodingForWordsLength5() {
        TestMixedPhoneNumberBuilder builder = new TestMixedPhoneNumberBuilder(3, 5);
        findCombinations(builder);
    }

    @Test
    public void testSingleWord() {
        String word = "01110-HOERS-11000";
        String digits = "011104637711000";
        convertAndAssert(digits,word);
    }

    @Test
    public void testSingleWord2() {
        String word = "100-OFF-011";
        String digits = "100633011";
        convertAndAssert(digits,word);
    }

    private void findCombinations(TestMixedPhoneNumberBuilder builder) {
        String digitalPhoneNumber = builder.getDigitalPhoneNumber();
        String baseEncodedCombination = builder.getOrignalNumberWithSeperator();
        convertAndAssert(digitalPhoneNumber, baseEncodedCombination);
    }

    private void convertAndAssert(String digitalPhoneNumber, String baseEncodedCombination) {
        IDigitalNumberToWordConvertor convertor = PhoneNumberConvertorFactory.getInstance().getPhoneNumberConvertor(digitalPhoneNumber);
        Set<String> allMatchingNumbers = convertor.getAllMatchingNumbers();
        Assert.assertTrue("Atleast one encoded number is found",allMatchingNumbers.size()>=0);
        Assert.assertTrue("Orignal Number "+baseEncodedCombination+" should be part of the combination set "+allMatchingNumbers,
                allMatchingNumbers.contains(baseEncodedCombination));
    }

}
