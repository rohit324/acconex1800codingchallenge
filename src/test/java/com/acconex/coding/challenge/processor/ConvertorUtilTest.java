package com.acconex.coding.challenge.processor;

import com.acconex.coding.challenge.common.BaseTest;
import com.acconex.coding.challenge.harnes.builders.TestBoundryPhoneNumberBuilder;
import com.acconex.coding.challenge.harnes.builders.TestMixedPhoneNumberBuilder;
import com.acconex.coding.challenge.harnes.builders.TestWordsOnlyPhoneNumberBuilder;
import com.acconex.coding.challenge.utils.DigitsEncoder;
import junit.framework.Assert;
import org.junit.Test;

import java.util.*;

/**
 * Created by rohit on 25/07/16.
 */
public class ConvertorUtilTest extends BaseTest{

    @Test
    public void testEmptyWordSet() {
        HashSet<Set<String>> emptySet = new HashSet<>();
        Set<String> combinationForWordSets = ConvertorUtil.createCombinationForWordSets(emptySet);
        Assert.assertNotNull(combinationForWordSets);
        org.junit.Assert.assertEquals(0,combinationForWordSets.size());
    }

    @Test
    public void testNullWordSet() {
        HashSet<Set<String>> emptySet = null;
        Set<String> combinationForWordSets = ConvertorUtil.createCombinationForWordSets(emptySet);
        Assert.assertNotNull(combinationForWordSets);
        Assert.assertEquals(0,combinationForWordSets.size());
    }

    @Test
    public void test1EmptyWordSet() {
        HashSet<Set<String>> collectionSet = new HashSet<>();
        HashSet<String> set1 = new HashSet<>();
        HashSet<String> set2 = new HashSet<>();
        set1.add("test");
        collectionSet.add(set1);
        collectionSet.add(set2);
        Set<String> combinationForWordSets = ConvertorUtil.createCombinationForWordSets(collectionSet);
        Assert.assertNotNull(combinationForWordSets);
        Assert.assertEquals(1,combinationForWordSets.size());
    }

    @Test
    public void test1EmptyWordSet2() {
        HashSet<Set<String>> collectionSet = new HashSet<>();
        HashSet<String> set1 = new HashSet<>();
        HashSet<String> set2 = new HashSet<>();
        set2.add("test");
        collectionSet.add(set1);
        collectionSet.add(set2);
        Set<String> combinationForWordSets = ConvertorUtil.createCombinationForWordSets(collectionSet);
        Assert.assertNotNull(combinationForWordSets);
        Assert.assertEquals(1,combinationForWordSets.size());
    }
    
    @Test
    public void testDictionarySetGeneration(){
        String digitalCombination = "10-69-6-9";
        Map<Integer, Set<String>> dictionaryMap = ConvertorUtil.generateDictionarySetsForEveryDigitalGroup(digitalCombination);
        Collection<Set<String>> values = dictionaryMap.values();
        ArrayList<Set<String>> setList = new ArrayList<>(values);

        // check the first group.
        Assert.assertEquals(1,setList.get(0).size());
        Assert.assertEquals("10",setList.get(0).iterator().next());

        // check the second group.
        Assert.assertEquals(5,setList.get(1).size());
        Assert.assertTrue("OW",setList.get(1).contains("OW"));

        // check the third group.
        Assert.assertEquals(1,setList.get(2).size());
        Assert.assertEquals("6",setList.get(2).iterator().next());

        // check the forth group.
        Assert.assertEquals(1,setList.get(3).size());
        Assert.assertEquals("9",setList.get(3).iterator().next());

    }

    @Test
    public void testDictionarySet() {
        String digitalCombination = "01110-46377-11000";
        Map<Integer, Set<String>> dictionaryMap = ConvertorUtil.generateDictionarySetsForEveryDigitalGroup(digitalCombination);
        Collection<Set<String>> values = dictionaryMap.values();
        ArrayList<Set<String>> setList = new ArrayList<>(values);

        // check the first group.
        Assert.assertEquals(1,setList.get(0).size());
        Assert.assertEquals("01110",setList.get(0).iterator().next());

        // check the second group.
        Assert.assertEquals(2,setList.get(1).size());
        Assert.assertTrue("GOERS",setList.get(1).contains("GOERS"));
        Assert.assertTrue("HOERS",setList.get(1).contains("HOERS"));

        // check the third group.
        Assert.assertEquals(1,setList.get(2).size());
        Assert.assertEquals("11000",setList.get(2).iterator().next());
    }

    @Test
    public void testCombinationGenerationForSet() {
        LinkedList<Set<String>> sets = new LinkedList<>();
        HashSet<String> set = new HashSet<>();
        set.add("01110");
        sets.add(set);

        set = new HashSet<>();
        set.add("GOERS");
        set.add("HOERS");
        sets.add(set);

        set = new HashSet<>();
        set.add("11000");
        sets.add(set);

        Set<String> combinationForWordSets = ConvertorUtil.createCombinationForWordSets(sets);
        Assert.assertEquals("Expecting 2 matches " + combinationForWordSets,2,combinationForWordSets.size());
        String match = "01110-GOERS-11000";
        Assert.assertTrue("Match not found in set " + combinationForWordSets,combinationForWordSets.contains(match));

        match = "01110-HOERS-11000";
        Assert.assertTrue("Match not found in set " + combinationForWordSets,combinationForWordSets.contains(match));
    }

    @Test
    public void testCombinationGenerationForDigitalSets() {
        LinkedList<Set<String>> sets = new LinkedList<>();
        Set<String> set1 = DigitsEncoder.getEncodedAlphabetsForADigit("2".charAt(0));
        sets.add(set1);

        Set<String> set2 = DigitsEncoder.getEncodedAlphabetsForADigit("2".charAt(0));
        sets.add(set2);

        Set<String> combinationForWordSets = ConvertorUtil.createCombinationForWordSets(sets);
        org.junit.Assert.assertEquals("[A-A, A-B, A-C, B-A, B-B, B-C, C-A, C-B, C-C]",combinationForWordSets.toString());
    }

    @Test
    public void testCombinationGenerationForDigitalSets2() {
        LinkedList<Set<String>> sets = new LinkedList<>();
        Set<String> set1 = DigitsEncoder.getEncodedAlphabetsForADigit("2".charAt(0));
        sets.add(set1);

        Set<String> set2 = DigitsEncoder.getEncodedAlphabetsForADigit("3".charAt(0));
        sets.add(set2);

        Set<String> combinationForWordSets = ConvertorUtil.createCombinationForWordSets(sets);
        org.junit.Assert.assertEquals("[A-D, A-E, A-F, B-D, B-E, B-F, C-D, C-E, C-F]",combinationForWordSets.toString());
    }

}
