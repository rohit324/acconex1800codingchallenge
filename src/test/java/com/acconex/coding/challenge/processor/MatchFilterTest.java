package com.acconex.coding.challenge.processor;

import com.acconex.coding.challenge.harnes.builders.TestBoundryPhoneNumberBuilder;
import com.acconex.coding.challenge.harnes.builders.TestMixedPhoneNumberBuilder;
import com.acconex.coding.challenge.harnes.builders.TestWordsOnlyPhoneNumberBuilder;
import junit.framework.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by rohit on 01/08/16.
 */
public class MatchFilterTest {

    @Test
    public void testRemoveMatchesOnlyBoundrySet() {
        HashSet<String> matchedSet = new HashSet<>();
        for(int i=0;i<5;i++) {
            TestMixedPhoneNumberBuilder builder = new TestBoundryPhoneNumberBuilder(2, i+1);
            matchedSet.add(builder.getDigitalNumberWithSeperator());
        }
        Set<String> cleanSet = new MatchFilter(matchedSet).filterResults();
        Assert.assertEquals(0,cleanSet.size());
    }

    @Test
    public void testRemoveMatchesOnlyWordSets(){
        HashSet<String> matchedSet = new HashSet<>();
        for(int i=0;i<4;i++) {
            TestMixedPhoneNumberBuilder builder = new TestWordsOnlyPhoneNumberBuilder(2, i+1);
            matchedSet.add(builder.getOrignalNumberWithSeperator());
        }
        Set<String> cleanSet = new MatchFilter(matchedSet).filterResults();
        Assert.assertEquals("MatchSet ::" + matchedSet +" is not same as cleaned set ::" +cleanSet,
                matchedSet.size(),cleanSet.size());
    }

}
