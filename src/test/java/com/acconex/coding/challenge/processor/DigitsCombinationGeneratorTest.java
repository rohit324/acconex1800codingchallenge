package com.acconex.coding.challenge.processor;

import com.acconex.coding.challenge.common.BaseTest;
import com.acconex.coding.challenge.common.Constants;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * Created by rohit on 24/07/16.
 */
public class DigitsCombinationGeneratorTest extends BaseTest{

    // TODO: Add null and 0 length strings.

    @Test
    public void testCombinationForSingleCharString(){
        String testString = "1";
        DigitsCombinationGenerator generator = new DigitsCombinationGenerator(testString);
        Set<String> combinationSet = generator.getAllCombinationOfDigits();
        Assert.assertEquals(1,combinationSet.size());
        Assert.assertEquals("1",combinationSet.iterator().next());
    }

    @Test
    public void testCombinationForBothBoundryDigits(){
        // todo :- remove hard coding for boundry
        String testString = "11";
        DigitsCombinationGenerator generator = new DigitsCombinationGenerator(testString);
        Set<String> combinationSet = generator.getAllCombinationOfDigits();
        Assert.assertEquals(1,combinationSet.size());
        Assert.assertEquals("11",combinationSet.iterator().next());
    }

    @Test
    public void testCombinationForBothEncodedDigits(){
        // todo :- remove hard coding for encoded.
        String testString = "23";
        DigitsCombinationGenerator generator = new DigitsCombinationGenerator(testString);
        Set<String> combinationSet = generator.getAllCombinationOfDigits();
        Assert.assertEquals(2,combinationSet.size());
        Assert.assertTrue("Expecting 23 to be part of the group" + combinationSet,combinationSet.contains("23"));
        Assert.assertTrue("Expecting 2"+ Constants.SEPERATOR+"3 to be part of the group",combinationSet.contains("2"+Constants.SEPERATOR+"3"));
    }

    @Test
    public void testCombinationForMultipleEncodedDigits(){
        // todo :- remove hard coding for encoded.
        String testString = "234";
        DigitsCombinationGenerator generator = new DigitsCombinationGenerator(testString);
        Set<String> combinationSet = generator.getAllCombinationOfDigits();
        double pow = Math.pow(2, testString.length() - 1);
        Assert.assertEquals(new Double(pow).intValue(),combinationSet.size());
    }

    @Test
    public void testCombinationForMixedDigits1(){
        // todo :- remove hard coding for encoded.
        String testString = "1112";
        DigitsCombinationGenerator generator = new DigitsCombinationGenerator(testString);
        Set<String> combinationSet = generator.getAllCombinationOfDigits();
        Assert.assertEquals("Expecting only 1 match but found "+combinationSet,2,combinationSet.size());
        Assert.assertTrue("1112",combinationSet.contains("1112"));
        Assert.assertTrue("111"+Constants.SEPERATOR+"2",combinationSet.contains("111"+Constants.SEPERATOR+"2"));
    }


    // TODO :- Need to build something which can do automated testing.
}
