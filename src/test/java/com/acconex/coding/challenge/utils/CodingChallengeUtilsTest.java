package com.acconex.coding.challenge.utils;

import com.acconex.coding.challenge.common.BaseTest;
import com.acconex.coding.challenge.exception.CodingChallengeRuntimeException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rohit on 24/07/16.
 */
public class CodingChallengeUtilsTest extends BaseTest{

    @Test(expected=CodingChallengeRuntimeException.class)
    public void testNullInput() {
        String testString = null;
        CodingChallengeUtils.removeSpacesAndPunctuations(testString);
    }


    @Test
    public void testRemoveSpacesAndPunctuations(){
        String testString = "!@12 <>,%^ 3 4\t\t7A\n\rju!{}[]().   as?";
        String expectedOutput = "12347Ajuas";
        String actualOutput = CodingChallengeUtils.removeSpacesAndPunctuations(testString);
        Assert.assertEquals("Expecting all spaces and punctuations to be removed from string " + testString,expectedOutput,actualOutput);
    }

}
