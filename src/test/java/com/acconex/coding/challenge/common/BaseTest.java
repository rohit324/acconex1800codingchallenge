package com.acconex.coding.challenge.common;

import com.acconex.coding.challenge.utils.FileUtils;

import java.io.FileNotFoundException;
import java.io.File;
/**
 * Created by rohit on 28/07/16.
 */
public class BaseTest {

    static {
        try {
            setUp();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void setUp() throws FileNotFoundException {
        String absolutePath = FileUtils.getAbsolutePath("logging.properties");
        File file = new File(absolutePath);
        System.setProperty("java.util.logging.config.file", file.getAbsolutePath());
    }

}
