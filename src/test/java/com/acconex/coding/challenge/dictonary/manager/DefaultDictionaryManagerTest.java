package com.acconex.coding.challenge.dictonary.manager;

import com.acconex.coding.challenge.common.BaseTest;
import com.acconex.coding.challenge.dictonary.facade.Dictionary;
import com.acconex.coding.challenge.dictonary.facade.DictionaryManager;
import com.acconex.coding.challenge.dictonary.exception.DictionaryException;
import junit.framework.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * Created by rohit on 25/07/16.
 */
public class DefaultDictionaryManagerTest extends BaseTest{

    DictionaryManager manager = DictionaryManager.getInstance();
    Dictionary dictionary = manager.getDictionary();


    @Test(expected=DictionaryException.class)
    public void testFileNotFound() throws DictionaryException {
        String nonExistingFile = "missingFile.txt";
        manager.loadNewDictionary(nonExistingFile);
    }

    @Test
    public void testDictionaryWords() throws DictionaryException {
        Set<String> allWordsOfLength = dictionary.getAllWordsOfLength(4);
        Assert.assertEquals(4456, allWordsOfLength.size());
    }


}
