package com.acconex.coding.challenge.harnes;

import com.acconex.coding.challenge.dictonary.facade.Dictionary;
import com.acconex.coding.challenge.dictonary.facade.DictionaryManager;
import com.acconex.coding.challenge.utils.CodingChallengeUtils;

import java.util.Random;
import java.util.Set;

/**
 * Created by rohit on 26/07/16.
 */
public class TestUtils {

    public static String createBoundryGroup(int length) {
        int wordLength = getRandomWordLength(length);
        char[] chars = new char[wordLength];
        for (int i=0;i<chars.length;i++) {
            int counter = getRandomCount();
            String boundryCharactor = CodingChallengeUtils.getBoundryCharactor(counter);
            chars[i] = boundryCharactor.charAt(0);
        }
        return new String(chars);
    }

    private static int getRandomWordLength(int length) {
        Random random = new Random();
        return random.nextInt(length)+1;
    }

    public static int getRandomCount() {
        return new Random().nextInt(2);
    }

    // todo :- not complete length but max length
    public static String createEncodableGroupFromDictionary(int maxlength) {
        Dictionary manager = DictionaryManager.getInstance().getDictionary();
        int length = getRandomWordLength(maxlength);
        Set<String> allWordsOfLength = manager.getAllWordsOfLength(length);
        int selectedWordIndex = new Random().nextInt(allWordsOfLength.size());
        String[] values = allWordsOfLength.toArray(new String[]{});
        return values[selectedWordIndex];
    }

}
