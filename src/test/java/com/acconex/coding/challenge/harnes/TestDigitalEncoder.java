package com.acconex.coding.challenge.harnes;

import com.acconex.coding.challenge.exception.CodingChallengeRuntimeException;
import com.acconex.coding.challenge.utils.DigitsEncoder;

import java.util.Map;
import java.util.Set;

/**
 * Created by rohit on 28/07/16.
 */
public class TestDigitalEncoder {

    public static String getIntegerForAlphabet(char alphabet) {
        // small map we can iterate everytime rather than creating a completely duplicate copy of the encodings.
        Set<Map.Entry<String, String>> entries = DigitsEncoder.getDigitMap().entrySet();
        for (Map.Entry<String, String> entry : entries) {
            String key = entry.getKey();
            String value = entry.getValue();
            String alpha = "" + alphabet;
            alpha = alpha.toUpperCase();
            if (value.contains(alpha)) {
                return key;
            }
        }
        throw new CodingChallengeRuntimeException("Not able to find decoding for alphabet" + alphabet);
    }
}
