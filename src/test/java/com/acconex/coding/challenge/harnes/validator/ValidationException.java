package com.acconex.coding.challenge.harnes.validator;

/**
 * Created by rohit on 27/07/16.
 */
public class ValidationException extends RuntimeException {

    public ValidationException() {
    }

    public ValidationException(String message) {
        super(message);
    }
}
