package com.acconex.coding.challenge.harnes.validator;

import com.acconex.coding.challenge.common.Constants;
import com.acconex.coding.challenge.dictonary.facade.Dictionary;
import com.acconex.coding.challenge.dictonary.facade.DictionaryManager;
import com.acconex.coding.challenge.utils.CodingChallengeUtils;
import junit.framework.Assert;

import java.util.Set;

/**
 * Created by rohit on 27/07/16.
 */
public class TestResultValidator {

    public static void validateResult(String orignalMobileNumber, Set<String> matches) {
        for (String match : matches) {
            assertEqualLength(orignalMobileNumber, match);
            String[] groups = match.split(Constants.SEPERATOR);
            for (String group : groups) {
                int length = group.length();
                if(length>1) {
                    validateMoreThanOneCharGroups(group);
                }
            }
            validateConsecutiveMatchNotFound(match);
        }
    }

    private static void validateConsecutiveMatchNotFound(String match) {
        String[] groups = match.split(Constants.SEPERATOR);
        if(groups.length<2) return;

        for(int i=1;i<groups.length;i++) {
            String group1 = groups[i - 1];
            String group2 = groups[i];
            if(group1.length() == 1 && group2.length() == 1) {
                if(isEndodedDigitType(group1) && isEndodedDigitType(group2)) {
                    org.junit.Assert.fail("2 consecutive unmatched digits for the match::" + match);
                }
            }
        }
    }

    private static void validateMoreThanOneCharGroups(String group) {
        boolean isDigitType = isDigitType(group);
        if(isDigitType) {
            validateGroupAsBoundryGroup(group);
        }
        else {
            validateWordGroup(group);
        }
    }

    private static boolean isEndodedDigitType(String group) {
            char firstChar = group.charAt(0);
            boolean isDigit = Character.isDigit(firstChar);
            boolean boundaryDigit = CodingChallengeUtils.isBoundaryDigit(firstChar);
            if(boundaryDigit) return false;
            return isDigit;
    }

    private static boolean isDigitType(String group) {
        char firstChar = group.charAt(0);
        return Character.isDigit(firstChar);
    }

    private static void validateWordGroup(String match) {
        Dictionary manager = DictionaryManager.getInstance().getDictionary();
        Set<String> allWords = manager.getAllWordsOfLength(match.length());
        Assert.assertTrue("Expecting dictionary match for the word " + match, allWords.contains(match));
    }

    private static void validateGroupAsBoundryGroup(String match) {
        char[] chars = match.toCharArray();
        for (char aChar : chars) {
            boolean boundaryDigit = CodingChallengeUtils.isBoundaryDigit(aChar);
            Assert.assertTrue("Expecting only boundry digits in the group " + match,boundaryDigit);
        }

    }

    private static void assertEqualLength(String orignalMobileNumber, String match) {
        Assert.assertEquals("Length of matched encoded number :: "  +
                        match + " must be equal to orignal number :: " + orignalMobileNumber,
                orignalMobileNumber.length(),match.replaceAll(Constants.SEPERATOR, "").length());
    }
}
