package com.acconex.coding.challenge.harnes.builders;

import com.acconex.coding.challenge.common.Constants;

import static com.acconex.coding.challenge.harnes.TestUtils.createBoundryGroup;

/**
 * Created by rohit on 25/07/16.
 */
public class TestBoundryPhoneNumberBuilder extends TestMixedPhoneNumberBuilder {

    public TestBoundryPhoneNumberBuilder(int noOfGroups, int maxGroupLength) {
        super(noOfGroups, maxGroupLength);
    }

    protected void generateWordedNumber() {
        StringBuilder builder = new StringBuilder();
        for(int i=0;i<noOfGroups;i++) {
            builder.append(createBoundryGroup(maxGroupLength));
            builder.append(Constants.SEPERATOR);
        }
        phoneNumberWithDictionaryWordsWithSeperator =  builder.toString();
    }
}
