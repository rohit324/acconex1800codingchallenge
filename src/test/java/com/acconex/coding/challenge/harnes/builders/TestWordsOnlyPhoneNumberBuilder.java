package com.acconex.coding.challenge.harnes.builders;

import com.acconex.coding.challenge.common.Constants;

import static com.acconex.coding.challenge.harnes.TestUtils.createEncodableGroupFromDictionary;

/**
 * Created by rohit on 26/07/16.
 */
public class TestWordsOnlyPhoneNumberBuilder extends TestMixedPhoneNumberBuilder {

    public TestWordsOnlyPhoneNumberBuilder(int noOfGroups, int maxGroupLength) {
        super(noOfGroups, maxGroupLength);
    }

    protected void generateWordedNumber() {
        StringBuilder builder = new StringBuilder();
        for(int i=0;i<noOfGroups;i++) {
            builder.append(createEncodableGroupFromDictionary(maxGroupLength));
            builder.append(Constants.SEPERATOR);
        }
        truncateSeperator(builder);
    }
}
