package com.acconex.coding.challenge.harnes.builders;

import com.acconex.coding.challenge.common.Constants;
import com.acconex.coding.challenge.harnes.TestDigitalEncoder;
import com.acconex.coding.challenge.utils.CodingChallengeUtils;

import static com.acconex.coding.challenge.harnes.TestUtils.createBoundryGroup;
import static com.acconex.coding.challenge.harnes.TestUtils.createEncodableGroupFromDictionary;

/**
 * Created by rohit on 25/07/16.
 */
public class TestMixedPhoneNumberBuilder {

    protected int noOfGroups;
    protected int maxGroupLength;
    protected String phoneNumberWithDictionaryWordsWithSeperator;

    public TestMixedPhoneNumberBuilder(int noOfGroups, int maxGroupLength) {
        this.noOfGroups = noOfGroups;
        this.maxGroupLength = maxGroupLength;
        generateWordedNumber();
    }

    public String getDigitalPhoneNumber() {
        char[] chars = getOrignalNumber().toCharArray();
        return convertAlphasToDigits(chars).replaceAll(Constants.SEPERATOR,"");
    }

    public static String convertAlphasToDigits(char[] chars) {
        for (int i = 0; i < chars.length; i++) {
            if(CodingChallengeUtils.isBoundaryDigit(chars[i]))continue;

            char aChar = chars[i];
            if(Constants.SEPERATOR.equals(""+aChar))continue;
            String integerForAlphabet = TestDigitalEncoder.getIntegerForAlphabet(aChar);
            chars[i] = integerForAlphabet.charAt(0);
        }
        return new String(chars);
    }

    public String getDigitalNumberWithSeperator() {
        return convertAlphasToDigits(getOrignalNumberWithSeperator().toCharArray());
    }

    protected void generateWordedNumber() {
        StringBuilder builder = new StringBuilder();
        for(int i=0;i<noOfGroups;i++) {
            if(i%2==0) {
                builder.append(createBoundryGroup(maxGroupLength));
            }else {
                builder.append(createEncodableGroupFromDictionary(maxGroupLength));
            }
            builder.append(Constants.SEPERATOR);
        }
        truncateSeperator(builder);
    }

    protected void truncateSeperator(StringBuilder builder) {
        String number = builder.toString();
        phoneNumberWithDictionaryWordsWithSeperator = number.substring(0,number.length()-1);
    }

    public String getOrignalNumber(){
        return removeSeperator(phoneNumberWithDictionaryWordsWithSeperator);
    }

    public String getOrignalNumberWithSeperator() {
        return phoneNumberWithDictionaryWordsWithSeperator;
    }

    private String removeSeperator(String number) {
        return number.replaceAll("//"+Constants.SEPERATOR,"");
    }


}
