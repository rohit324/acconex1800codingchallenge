package com.acconex.coding.challenge.groups;

import com.acconex.coding.challenge.common.BaseTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rohit on 24/07/16.
 */
public class DigitsGroupCreatorTest extends BaseTest {

    @Test
    public void testBoundryDigitsCreation() {
        String boundryDigitsGroup = "01010";
        IDigitGroup group = DigitsGroupCreator.createGroup(boundryDigitsGroup);
        Assert.assertTrue("Expecting instance of class " + BoundryDigitsGroup.class.getSimpleName(),
                group instanceof BoundryDigitsGroup);
    }

    @Test
    public void testEncodableDigitsCreation() {
        String mixedDigitsGroup = "67332";
        IDigitGroup group = DigitsGroupCreator.createGroup(mixedDigitsGroup);
        Assert.assertTrue("Expecting instance of class " + EncodableDigitsGroup.class.getSimpleName(),
                group instanceof EncodableDigitsGroup);
    }

    @Test
    public void testMixedDigitsCreation() {
        String mixedDigitsGroup = "107332";
        IDigitGroup group = DigitsGroupCreator.createGroup(mixedDigitsGroup);
        Assert.assertTrue("Expecting instance of class " + MixedDigitsGroup.class.getSimpleName(),
                group instanceof MixedDigitsGroup);
    }
}
