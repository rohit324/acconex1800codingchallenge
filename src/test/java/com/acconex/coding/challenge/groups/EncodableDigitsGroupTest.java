package com.acconex.coding.challenge.groups;

import com.acconex.coding.challenge.common.BaseTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * Created by rohit on 24/07/16.
 */
public class EncodableDigitsGroupTest extends BaseTest {

    // TODO :- Need to add further test cases to check if encoding works for each and every word.
    @Test
    public void testGetMatchingWordsOfSize2() {
        String digits = "69";
        IDigitGroup group = DigitsGroupCreator.createGroup(digits);
        Assert.assertTrue("Expecting group of type " + EncodableDigitsGroup.class.getSimpleName() , group instanceof EncodableDigitsGroup);

        Set<String> matchingWordSet = group.getMatchingDictionaryOrBoundryWords();
        Assert.assertEquals(5,matchingWordSet.size());
        Assert.assertTrue("Expecting word match for OW",matchingWordSet.contains("OW"));
        Assert.assertTrue("Expecting word match for OX",matchingWordSet.contains("OX"));
        Assert.assertTrue("Expecting word match for OY",matchingWordSet.contains("OY"));
        Assert.assertTrue("Expecting word match for NY",matchingWordSet.contains("NY"));
        Assert.assertTrue("Expecting word match for MY",matchingWordSet.contains("MY"));
    }

    @Test
    public void testGetMatchingWordsOfSize3() {
        String digits = "662";
        IDigitGroup group = DigitsGroupCreator.createGroup(digits);
        Assert.assertTrue("Expecting group of type " + EncodableDigitsGroup.class.getSimpleName() , group instanceof EncodableDigitsGroup);

        Set<String> matchingWordSet = group.getMatchingDictionaryOrBoundryWords();
        Assert.assertEquals(4,matchingWordSet.size());
        Assert.assertTrue("Expecting word match for NOB",matchingWordSet.contains("NOB"));
        Assert.assertTrue("Expecting word match for MNA",matchingWordSet.contains("MNA"));
        Assert.assertTrue("Expecting word match for MOB",matchingWordSet.contains("MOB"));
        Assert.assertTrue("Expecting word match for MOA",matchingWordSet.contains("MOA"));
    }

    @Test
    public void testGetMatchingWordsOfSize4() {
        String digits = "7683";
        IDigitGroup group = DigitsGroupCreator.createGroup(digits);
        Assert.assertTrue("Expecting group of type " + EncodableDigitsGroup.class.getSimpleName() , group instanceof EncodableDigitsGroup);

        Set<String> matchingWordSet = group.getMatchingDictionaryOrBoundryWords();
        Assert.assertEquals(4,matchingWordSet.size());
        Assert.assertTrue("Expecting word match for POTE",matchingWordSet.contains("POTE"));
        Assert.assertTrue("Expecting word match for POUF",matchingWordSet.contains("POUF"));
        Assert.assertTrue("Expecting word match for ROTE",matchingWordSet.contains("ROTE"));
        Assert.assertTrue("Expecting word match for ROVE",matchingWordSet.contains("ROVE"));
    }

    @Test
    public void testGetMatchingWordsOfSize5() {
        String digits = "72769";
        IDigitGroup group = DigitsGroupCreator.createGroup(digits);
        Assert.assertTrue("Expecting group of type " + EncodableDigitsGroup.class.getSimpleName() , group instanceof EncodableDigitsGroup);
        Set<String> matchingWordSet = group.getMatchingDictionaryOrBoundryWords();
        Assert.assertEquals(1,matchingWordSet.size());
        Assert.assertTrue("Expecting word match for SCROW",matchingWordSet.contains("SCROW"));
    }
}
