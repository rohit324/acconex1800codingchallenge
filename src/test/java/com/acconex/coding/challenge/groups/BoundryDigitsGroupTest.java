package com.acconex.coding.challenge.groups;

import com.acconex.coding.challenge.common.BaseTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * Created by rohit on 24/07/16.
 */
public class BoundryDigitsGroupTest extends BaseTest{

    @Test
    public void testBoundryGroup() {
        String testGroup = "10101";
        IDigitGroup group = DigitsGroupCreator.createGroup(testGroup);
        Assert.assertTrue("Expecting group of type" + group.getClass().getSimpleName(),group instanceof BoundryDigitsGroup);
        Set<String> matchingDictionaryWords = group.getMatchingDictionaryOrBoundryWords();
        Assert.assertEquals("Expecting only 1 match",1,matchingDictionaryWords.size());
        Assert.assertTrue("Exepecting " + testGroup +" to be in the match.",matchingDictionaryWords.contains(testGroup));
    }

}
