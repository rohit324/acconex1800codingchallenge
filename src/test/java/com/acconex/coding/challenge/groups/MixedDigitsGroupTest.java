package com.acconex.coding.challenge.groups;

import com.acconex.coding.challenge.common.BaseTest;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rohit on 24/07/16.
 */
public class MixedDigitsGroupTest extends BaseTest {

    @Test
    public void testMixedGroup() {
        String mixedGroupString = "1012";
        IDigitGroup group = DigitsGroupCreator.createGroup(mixedGroupString);
        Assert.assertTrue("Expecting group of type" + group.getClass().getSimpleName(),
                group instanceof MixedDigitsGroup);
        Assert.assertEquals(null,group.getMatchingDictionaryOrBoundryWords());
    }
}
