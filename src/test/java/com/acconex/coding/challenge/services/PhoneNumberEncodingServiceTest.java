package com.acconex.coding.challenge.services;

import com.acconex.coding.challenge.common.BaseTest;
import com.acconex.coding.challenge.dictonary.facade.Dictionary;
import com.acconex.coding.challenge.dictonary.facade.DictionaryManager;
import com.acconex.coding.challenge.exception.InputValidationException;
import com.acconex.coding.challenge.harnes.TestDigitalEncoder;
import com.acconex.coding.challenge.harnes.builders.TestMixedPhoneNumberBuilder;
import com.acconex.coding.challenge.harnes.validator.TestResultValidator;
import com.acconex.coding.challenge.services.CLI.exception.CommandException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;
import java.util.Set;

/**
 * Created by rohit on 25/07/16.
 */
public class PhoneNumberEncodingServiceTest extends BaseTest{

    @Test
    public void testSimpleCaseNoOutput() throws InputValidationException {
        String testNumber = "101";
        PhoneNumberEncodingService service = getPhoneNumberService(testNumber);
        String encodedPhoneNumbers = service.getEncodedPhoneNumbers();
        Assert.assertNull("Expecting no match but found " + encodedPhoneNumbers,encodedPhoneNumbers);
    }

    private PhoneNumberEncodingService getPhoneNumberService(String testNumber) throws InputValidationException {
        return new PhoneNumberEncodingService(testNumber);
    }

    @Test
    public void testSimpleMatchFormattedOutput() throws InputValidationException {
        Dictionary dictionary = DictionaryManager.getInstance().getDictionary();
        Set<String> allWordsOfLength = dictionary.getAllWordsOfLength(1);
        Assert.assertTrue("Dictionary must conatin atleast 1 word with length one to run the test",allWordsOfLength.size()>0);
        String word = allWordsOfLength.iterator().next();
        String integerForAlphabet = TestDigitalEncoder.getIntegerForAlphabet(word.charAt(0));
        String testNumber = "1"+ integerForAlphabet +"1";
        PhoneNumberEncodingService service = getPhoneNumberService(testNumber);
        String encodedPhoneNumbers = service.getEncodedPhoneNumbers();
        Assert.assertEquals("1-"+word+"-1",encodedPhoneNumbers.replaceAll("\\s",""));
    }

    @Test
    public void testPhoneNumeber() throws InputValidationException {
        TestMixedPhoneNumberBuilder builder = new TestMixedPhoneNumberBuilder(2, 5);
        String digitalPhoneNumber = builder.getDigitalPhoneNumber();
        validateResult(digitalPhoneNumber);
    }


    @Test
    public void testSmallSetOfPhoneNumbers() throws InputValidationException {
        for(int i=0;i<100;i++) {
            TestMixedPhoneNumberBuilder builder = new TestMixedPhoneNumberBuilder(3, 5);
            String digitalPhoneNumber = builder.getDigitalPhoneNumber();
            validateResult(digitalPhoneNumber);
        }
    }

    @Test(expected=InputValidationException.class)
    public void testInvalidInputException() throws InputValidationException {
        String testNumber = "ABC";
        PhoneNumberEncodingService service = getPhoneNumberService(testNumber);
        service.getEncodedPhoneNumbers();
    }

    @Test
    public void testLargeSetOfRandomPhoneNumbers() throws InputValidationException {
        for(int i=0;i<2500;i++) {
            int goupCount = new Random().nextInt(3)+1;
            int groupSize = new Random().nextInt(5)+1;
            TestMixedPhoneNumberBuilder builder = new TestMixedPhoneNumberBuilder(goupCount, groupSize);
            String digitalPhoneNumber = builder.getDigitalPhoneNumber();
            validateResult(digitalPhoneNumber);
        }
    }

    private void validateResult(String digitalPhoneNumber) throws InputValidationException {
        PhoneNumberEncodingService service = getPhoneNumberService(digitalPhoneNumber);
        Set<String> allMatches = service.getAllMatches();
        TestResultValidator.validateResult(digitalPhoneNumber, allMatches);
    }
}
