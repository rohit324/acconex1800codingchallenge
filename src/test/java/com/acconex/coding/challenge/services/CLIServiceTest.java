package com.acconex.coding.challenge.services;

import com.acconex.coding.challenge.dictonary.exception.DictionaryException;
import com.acconex.coding.challenge.dictonary.facade.DictionaryManager;
import com.acconex.coding.challenge.services.CLI.CLIService;
import com.acconex.coding.challenge.services.CLI.CommandConstants;
import com.acconex.coding.challenge.services.CLI.exception.CommandException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import java.nio.file.Files;
import java.util.HashSet;
import static com.acconex.coding.challenge.services.CLI.CommandConstants.*;

/**
 * Created by rohit on 30/07/16.
 */
public class CLIServiceTest {

    private static HashSet<String> outputSet = new HashSet<>();
    String phoneNumner = "10101";

    @Before
    public void setUpOutStreamProcessor(){
        outputSet.clear();
        System.setOut(new PrintStream(System.out) {
            public void println(String s) {
                org.junit.Assert.assertTrue("Output Set does not contain the expected " +
                        "String::" + s, isOutputExpected(s));
            }
        });
    }

    @AfterClass
    public static void tearDown() throws DictionaryException {
        System.setOut(System.out);
        DictionaryManager.getInstance().resetDictionary();
    }

    private static boolean isOutputExpected(String output) {
        if(outputSet.contains(output)) return true;
        for (String expectedOut : outputSet) {
            if(output.trim().startsWith(expectedOut.trim())) return true;
        }
        return false;
    }

    @Test
    public void testSimpleInputNoMatch() throws Exception {
        outputSet.add(NO_MATCH+phoneNumner);
        outputSet.add(PROCESSING+phoneNumner);
        String[] args = {phoneNumner};
        CLIService.main(args);
    }

    @Test
    public void testSimpleInputMatch() throws Exception {
        outputSet.add(PROCESSING+"2");
        outputSet.add(MATCHES+"2");
        outputSet.add(START);
        outputSet.add("A");
        outputSet.add(END);
        DictionaryManager.getInstance().resetDictionary();
        String[] args = {"2"};
        CLIService.main(args);
    }

    @Test
    public void testSimpleFileInput() throws Exception {
        outputSet.add(NO_MATCH+phoneNumner);
        File file = new File("tempInput.txt");
        file.createNewFile();
        Files.write(file.toPath(), phoneNumner.getBytes());
        String[] args = {"-i",file.getAbsolutePath()};
        CLIService.main(args);
        file.delete();
    }

    @Test(expected=CommandException.class)
    public void testSimpleFileInputError() throws Exception {
        outputSet.add(ERROR_INPUT_FILE_NOT_FOUND);
        File file = new File("nonExistingFile.txt");
        String[] args = {"-i",file.getAbsolutePath()};
        CLIService.main(args);
    }

    @Test(expected=CommandException.class)
    public void testSimpleFileNoFileArg() throws Exception {
        outputSet.add(ERROR_MISSING_INPUT_FILE_PARAM);
        String[] args = {"-i",""};
        CLIService.main(args);
    }

    @Test
    public void testSimpleDictionaryInput() throws Exception {
        String phoneNumber = "10101";
        outputSet.add(CommandConstants.PROCESSING+phoneNumber);
        outputSet.add(CommandConstants.NO_MATCH+phoneNumber);
        File file = new File("tempDictionary.txt");
        file.createNewFile();
        Files.write(file.toPath(), "test".getBytes());
        String[] args = {"-d",file.getAbsolutePath(), phoneNumber};
        CLIService.main(args);
        file.delete();
    }

    @Test(expected=CommandException.class)
    public void testMissingDictionaryPath() throws Exception {
        outputSet.add(ERROR_DIC_FILE_NOT_FOUND);
        File file = new File("tempDirctoryMissing.txt");
        String[] args = {"-d",file.getAbsolutePath(),"10101"};
        CLIService.main(args);
    }

    @Test
    public void testNoValueForPath() throws Exception {
        outputSet.add(ERROR_DIC_PATH_MANDATORY);
        File file = new File("tempDirMissing.txt");
        file.createNewFile();
        String[] args = {"-d",""};
        CLIService.main(args);
        file.delete();
    }

    @Test(expected=CommandException.class)
    public void testloadingDictionary() throws Exception {
        outputSet.add("Error loading dictionary , Error Details :- Only one word per line is allowed for dictionary.");
        File file = new File("tempDir.txt");
        file.createNewFile();
        Files.write(file.toPath(), "more than one word per line".getBytes());
        String[] args = {"-d",file.getAbsolutePath(),"10101"};
        CLIService.main(args);
        file.delete();
    }

    @Test(expected=CommandException.class)
    public void testInvalidInput() throws Exception {
        String[] args = {"-r"};
        CLIService.main(args);
    }

    @Test(expected=CommandException.class)
    public void testInvalidInputException() throws Exception {
        outputSet.add("Processing the Phone number::ABC");
        String[] args = {"ABC"};
        CLIService.main(args);
    }

}
