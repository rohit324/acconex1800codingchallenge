# README #

# * Quick summary #

This repository holds the source code for 1-800-Coding Challenge problem for Acconex Programing Interview

## Problem Statement ##

A program that will show a user possible matches for a list of provided phone numbers.

Following conditions and constraints are to be fulfilled to by the program :- 

1. The program should output all possible word replacements from a dictionary.
 
2. The program should try to replace every digit of the provided phone number with a letter from a dictionary word; however, if no match can be made, a single digit can be left as is at that point. No two consecutive digits can remain unchanged and the program should skip over a number (producing no output) if a match cannot be made.

3. The program should allow the user to set a dictionary with the -d command-line option, but it's fine to use a reasonable default for your system. The dictionary is expected to have one word per line.

4. All punctuation and whitespace should be ignored in both phone numbers and the dictionary file. The program should not be case sensitive, letting "a" == "A". Output should be capital letters and digits separated at word boundaries with a single dash (-), one possible word encoding per line.


For example :- 

Lets consider a simple use case where a sample dictionary contains only 2 words 'CALL' and 'ME'
The encoding of the numbers are as follows :- 

![Screen Shot 2016-08-01 at 8.00.09 am.png](https://bitbucket.org/repo/yBbeAo/images/2203629752-Screen%20Shot%202016-08-01%20at%208.00.09%20am.png)
![Screen Shot 2016-08-01 at 8.00.17 am.png](https://bitbucket.org/repo/yBbeAo/images/3578874982-Screen%20Shot%202016-08-01%20at%208.00.17%20am.png)

Following are the sample input and output based on the above conditions :- 

For 100225563 
Output:- 1-00-CALL-ME


For 122556300
Output:- 1-CALL-ME-00


For 122550063
Output:- 1-CALL-00-ME


For 2100-225500
Output:- 2-1-00-CALL-00

For 2100-2225500
Output:- 2-1-00-2-CALL-00

For 2100-22225500
*Output:- No match found*

**since 22 are consecutive before a match is present for 2255.**

# Coding Challenge Selection Reasons #

1. Both the problems seemed equally challenging at the first glance but 1800 coding challenge felt like an ideal candidate for OOPS.

2. I could visualize different modules of the program very easily like Dictionary Management, CLI Service, Processor, Matcher.

3. As I could visualize different module, it was easier to adopt test driven approach module by module and finally testing the complete integration.

4. Since I had to finish the problem in 1 week, I felt I can display my strongs OOPs understanding through this  problem within that time.

# Approach & Design #

1. First the problem was broken up in various different service modules like :-
 
	a. Dictionary Management :- would provide all dictionary management services.

	b. Services Module:- would provide integration with command line interface and java service endpoint.

	c. Core Processing module :- will process the input numbers and create matches corresponding to them.


2. Core Processing module uses the following algorithm for finding the match :-
 
	a. First all the combinations are found for an input number, for example if number is 123 valid combinations are 1-2-3, 1-23, 12-3 and 123.

	b. These combinations are run against the dictionary matcher which convert digits to corresponding words.

	c. Filtering of the combinations which has only boundary words, also filtering of consecutive unmatched digits.

# Running the program #

Pr-requisites :- 

JAVA :- JDK 1.8

Maven :- 3.3.9 was used 

Running :- "mvn compile" before running the CLI commands

Command Line Interface :- 

1. With a single input phone number.

mvn exec:java -Djava.util.logging.config.file=logging.properties -Dexec.args="22.2256"

2. With multiple input phone number.

mvn exec:java -Djava.util.logging.config.file=logging.properties -Dexec.args="22.2256 18002245 1800"

3. With input from a file.

mvn exec:java -Djava.util.logging.config.file=logging.properties -Dexec.args="-i input.data"

*Please not the path should be fully qualified file path.*

4. With dictionary and a file and a number 

mvn exec:java -Djava.util.logging.config.file=logging.properties -Dexec.args="-d dictionary.data -i input.data 22 5"

# Reports and Builds #

1. The builds for the project can be accessed at the following location :- 

https://circleci.com/bb/rohit324/acconex1800codingchallenge

## Report Links  ##

1. Latest Consolidated project report can be accessed at https://60-ea481b1a-967c-4da0-a6e5-3e4795dde3e8-bb.circle-artifacts.com/0//tmp/circle-junit.0rGgBEy/index.html

You can use the following gihub credentials for accessing the detailed build reports in circleCI :- 

Login URL :- https://circleci.com/

UserName :- rohit1800acconex

Password :- acconex12

And the reports can be accessed from the artifacts tab by navigating into any of the builds.

### Report Highlights ###

1. 95% code coverage.
2. Overall code complexity of 1.98 with maximum as 5.2 for one class.

## Running maven test with Reports locally ##

After checkout run the command "mvn site" and the reports will be generated in target/site folder.

These tests can directly be run on cloud by logging into the Circle CI for which the instructions are provided above.